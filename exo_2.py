def echange(g1, g2, g3=10):
    return g3, g2, g1


def affiche(g1, g2, g3):
    message = """\
    *** Valeur des gobelets ***
    Gobelet n°1 = {}
    Gobelet n°2 = {}
    Gobelet n°3 = {}\n"""
    print(message.format(g1, g2, g3))


gob1 = int(input("Initialisez le gobelet n°1 : "))
gob2 = int(input("Initialisez le gobelet n°2 : "))
gob3 = int(input("Initialisez le gobelet n°3 : "))

gob1, gob2, gob3 = echange(g1=gob1, g2=gob2, g3=gob3)
affiche(gob1, gob2, gob3)
