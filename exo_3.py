def nombre_au_hasard():
    """
    Tire un nombre au hasard et propose à l'utilisateur de le deviner

    Pas de valeur de retour
    """
    from random import randint
    # Retourne un nombre aléatoire compris entre 1 et 100
    hasard = randint(1, 100)
    ok = True
    compteur = 0
    while ok:
        rep = int(input("Entrez un nombre : "))
        compteur += 1
        if rep > hasard:
            print("Trop grand")
        elif rep < hasard:
            print("Trop petit")
        else:
            ok = False
    print("Bravo !")
    if compteur == 1:
        print("Vous avez trouvé au premier essai !")
    else:
        message = "Vous avez trouvé après {} essais."
        print(message.format(compteur))


if __name__ == "__main__":
    nombre_au_hasard()
